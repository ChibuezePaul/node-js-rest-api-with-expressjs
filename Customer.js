const Joi = require('joi');
class Customer {
  constructor() {
    this.customerAcct = [
    {
      id: 1,
      success: true,
      message: 'Alhaja Rukayat Kolapo',
      bankName: 'UNITED BANK FOR AFRICA'
    },
    {
      id: 2,
      success: true,
      message: 'Mercy Akinkotu',
      bankName: 'FIDELITY BANK'
    },
    {
      id: 3,
      success: true,
      message: 'Amos Okomayin',
      bankName: 'UNION BANK'
    }
  ]
   }
  

  findAllCustomers() {
    return this.customerAcct;
  }

  findCustomer(req, res) {
    return this.customerExists(req, res);
  }

  createCustomer(customer) {
    const { error } = this.validateCustomer(customer); 
    if (error) return res.status(400).send(error.details[0].message);

    const customerBankDetails = {
      id: this.customerAcct.length + 1,
      success: true,
      message: customer.message,
      bankName: customer.bankName
    }

    this.customerAcct.push(customerBankDetails);

    return customerBankDetails;
  }

  updateCustomer(req, res) {
    const customerBankDetails = this.customerExists(req, res);
    const { error } = this.validateCustomer(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    const { message, bankName } = req.body;
    customerBankDetails.message = message;
    customerBankDetails.bankName = bankName;
    this.customerAcct.push(customerBankDetails);
    return customerBankDetails;
  }

  deleteCustomer(req, res) {
    const customerBankDetails = this.customerExists(req, res);
    const index = this.customerAcct.indexOf(customerBankDetails);
    this.customerAcct.splice(index, 1);
    return customerBankDetails;
  }

  validateCustomer(customer) {
    const schema = {
      // id: Joi.number().not().required(),
      // success: Joi.boolean().not().required(),
      bankName: Joi.string().min(3).required(),
      message: Joi.string().min(3).required()
    };

    return Joi.validate(customer, schema);
  }

  customerExists(req, res) {
    const customerBankDetails = this.customerAcct.find(c => c.id === parseInt(req.params.id));

    if (!customerBankDetails) return res.status(404).send('Customer Not Found!');

    return customerBankDetails;
  }
}

module.exports = Customer;