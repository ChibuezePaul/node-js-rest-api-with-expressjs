const express = require('express');
const Customer = require('./Customer')


const customer = new Customer();
const router = express.Router();

router.get('/', (req, res) => {
  res.send(customer.findAllCustomers());
});

router.get('/:id', (req, res) => {
  res.send(customer.findCustomer(req, res));
});

router.post('/', (req, res) => {
  res.send(customer.createCustomer(req.body));
});

router.put('/:id', (req, res) => {
  res.json(customer.updateCustomer(req, res));
});

router.delete('/:id', (req, res) => {
  res.send(customer.deleteCustomer(req, res));
});

module.exports = router;