const express = require('express');
const customerRouter = require('./customerRouter');
const app = express();
app.use(express.json());

app.use('/customers', customerRouter);

app.get('/corporate/transfer/:acctNumber/:bankName/nameEnquiry', (req, res) => {
  const customerBankDetails = customerAcct.find(c => c.bankName === req.params.bankName.toUpperCase());
  if (!customerBankDetails) return res.status(404).send('Customer Not Found!')

  res.send(customerBankDetails)
});

app.all('*', (req, res) => {
  res.status(403).send('Error: Unauthorized');
});

const port = process.env.PORT || 3000;

app.listen(port, () => console.log(`Service Listening on port ${port}...`));




